# FirstWinger2

## Todo

- [X] fix sound bug
- - [X] implement sound queue
- [X] fix bomb bug
- [X] categorize scripts
- [X] change boss pattern
- - [X] implement beam
- [X] Add Audio mixer
- [X] fix player moving bug
- [X] fix guided missile target bug
- [X] implement addon
- - [X] implement test version
- - [X] convert into online version
- [X] fix no damage bug
- [X] implement boss HP UI
- [X] implement spreading items when player dead
- [X] fix solo mode gameover bug
- [X] implement player life
- [X] fix camera out range of bullet
- [X] change game control type
- [X] implement sound
