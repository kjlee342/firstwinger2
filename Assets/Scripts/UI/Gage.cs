﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gage : MonoBehaviour
{
    [SerializeField]
    Slider slider;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void SetHP(float currentValue, float maxValue)
    {
        if (maxValue < currentValue)
        {
            currentValue = maxValue;
        }

        slider.value = currentValue / maxValue;
    }
}
