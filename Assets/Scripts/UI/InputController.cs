﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController
{
    public void UpdateInput()
    {
        if (SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().CurrentGameState != GameState.Running)
        {
            return;
        }

        UpdateKeyboard();
        // UpdateMouse();
    }

    void UpdateKeyboard()
    {
        Vector3 moveDirection = Vector3.zero;

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            moveDirection.y = 1;
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            moveDirection.x = -1;
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            moveDirection.y = -1;
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            moveDirection.x = 1;
        }

        Player player = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().Hero;

        if (Input.GetKeyDown(KeyCode.LeftShift) && player.AccurateMode == false)
        {
            player.AccurateMode = true;
        }

        if (Input.GetKeyUp(KeyCode.LeftShift) && player.AccurateMode)
        {
            player.AccurateMode = false;
        }

        player.ProcessInput(moveDirection);

        if (Input.GetKey(KeyCode.X))
        {
            SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().Hero.FireBomb();
        }

        if (Input.GetKey(KeyCode.Z))
        {
            SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().Hero.Fire();
        }
    }

    void UpdateMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().Hero.Fire();
        }

        if (Input.GetMouseButtonDown(1))
        {
            SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().Hero.FireBomb();
        }
    }
}
