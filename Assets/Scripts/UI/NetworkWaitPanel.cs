﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class NetworkWaitPanel : BasePanel
{
    const float TextUpdateinterval = 0.15f;
    const string LoadingTextValue = "...";

    float LastUpdateTime = 0.0f;
    int TextIndex = 0;

    [SerializeField]
    Text WatingText;

    [SerializeField]
    RectTransform SelfPosition;

    float PositionBackup = 0f;

    public override void InitializePanel()
    {
        base.InitializePanel();

        if (((FWNetworkManager)FWNetworkManager.singleton).isServer)
        {
            Show();

            PositionBackup = SelfPosition.anchoredPosition.x;
        }
    }

    public override void Show()
    {
        base.Show();

        SelfPosition.anchoredPosition = new Vector2(0, SelfPosition.anchoredPosition.y);
    }

    public override void Close()
    {
        base.Close();

        SelfPosition.anchoredPosition = new Vector2(PositionBackup, SelfPosition.anchoredPosition.y);
    }

    public override void UpdatePanel()
    {
        float now = Time.time;
        if (TextUpdateinterval < now - LastUpdateTime)
        {
            WatingText.text = LoadingTextValue.Substring(0, TextIndex++);

            if (LoadingTextValue.Length < TextIndex)
            {
                TextIndex = 0;
            }

            LastUpdateTime = now;
        }
    }

    public void OnStartSoloMode()
    {
        InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();
        if (inGameSceneMain.CurrentGameState == GameState.None)
        {
            inGameSceneMain.GameStart();
        }

        Close();
    }
}
