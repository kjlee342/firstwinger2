﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDamage : MonoBehaviour
{
    enum DamageState : int
    {
        None = 0,
        SizeUp,
        Display,
        FadeOut,
    }

    [SerializeField]
    DamageState damageState = DamageState.None;

    const float SizeUpDuration = 0.1f;
    const float DisplayDuration = 0.5f;
    const float FadeOutDuration = 0.2f;

    [SerializeField]
    Text damageText;

    Vector3 CurrentVelocity;

    float DisplayStartTime;
    float FadeOutStartTime;

    public string FilePath
    {
        get;
        set;
    }

    private void Update()
    {
        UpdateDamage();
    }

    public void ShowDamage(int damage, Color color)
    {
        damageText.color = color;
        damageText.text = damage.ToString();
        DamageReset();
        damageState = DamageState.SizeUp;
    }

    private void DamageReset()
    {
        transform.localScale = Vector3.zero;
        Color newColor = damageText.color;
        newColor.a = 1.0f;
        damageText.color = newColor;
    }

    void UpdateDamage()
    {
        if (damageState == DamageState.None)
        {
            return;
        }

        float nowTime = Time.time;
        switch (damageState)
        {
            case DamageState.SizeUp:
                {
                    transform.localScale = Vector3.SmoothDamp(transform.localScale, Vector3.one, ref CurrentVelocity, SizeUpDuration);

                    if (transform.localScale == Vector3.one)
                    {
                        damageState = DamageState.Display;
                        DisplayStartTime = nowTime;
                    }
                }
                break;

            case DamageState.Display:
                {
                    if (DisplayDuration < nowTime - DisplayStartTime)
                    {
                        damageState = DamageState.FadeOut;
                        FadeOutStartTime = nowTime;
                    }
                }
                break;

            case DamageState.FadeOut:
                {
                    Color newColor = damageText.color;
                    newColor.a = Mathf.Lerp(1, 0, (nowTime - FadeOutStartTime) / FadeOutDuration);
                    damageText.color = newColor;

                    if (newColor.a == 0)
                    {
                        damageState = DamageState.None;
                        SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().DamageManager.Remove(this);
                    }
                }
                break;

            default:
                break;
        }
    }
}
