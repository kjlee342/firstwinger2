﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHUD : MonoBehaviour
{
    [SerializeField]
    Gage HPGage;

    [SerializeField]
    Actor OwnerPlayer;

    Transform OwnerTransform;
    Transform SelfTransform;

    private void Start()
    {
        SelfTransform = transform;
    }

    public void Initialize(Actor actor, bool follow = true)
    {
        OwnerPlayer = actor;
        if (follow)
        {
            OwnerTransform = actor.transform;
        }
    }

    private void Update()
    {
        UpdatePosition();
        UpdateHP();
    }

    void UpdatePosition()
    {
        if (OwnerTransform != null)
        {
            SelfTransform.position = Camera.main.WorldToScreenPoint(OwnerTransform.position);
        }
    }

    void UpdateHP()
    {
        if (OwnerPlayer != null)
        {
            if (OwnerPlayer.gameObject.activeSelf == false)
            {
                gameObject.SetActive(OwnerPlayer.gameObject.activeSelf);
            }

            HPGage.SetHP(OwnerPlayer.HPCurrent, OwnerPlayer.HPMax);
        }
    }
}
