﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarningPanel : BasePanel
{
    const int soundPriority = 100;

    const float BGColorVariationRate = 0.8f;
    const float MaxMoveTime = 5.0f;
    const float StayTime = 2.0f;

    [SerializeField]
    Image BackgroundImage;

    float BGImageColorMax;

    enum WarningUIPhase : int
    {
        In = 0,
        Stay,
        Out,
        End,
        Alert,
    }

    WarningUIPhase Phase = WarningUIPhase.Alert;

    [SerializeField]
    RectTransform CanvaseRectTransform;

    [SerializeField]
    RectTransform TextBackRectTransform;

    float MoveStartTime;
    float CurrentPosX;

    public override void InitializePanel()
    {
        base.InitializePanel();

        BGImageColorMax = BackgroundImage.color.a;
        Color color = BackgroundImage.color;
        color.a = 0f;
        BackgroundImage.color = color;

        Vector2 position = TextBackRectTransform.anchoredPosition;
        position.x = CanvaseRectTransform.sizeDelta.x;
        CurrentPosX = CanvaseRectTransform.sizeDelta.x;
        TextBackRectTransform.anchoredPosition = position;

        MoveStartTime = Time.time;
        Close();
    }

    public override void UpdatePanel()
    {
        base.UpdatePanel();

        UpdateColor();
        UpdateMove();
    }

    void UpdateColor()
    {
        Color color = BackgroundImage.color;
        color.a = Mathf.PingPong(Time.time * BGColorVariationRate, BGImageColorMax);
        BackgroundImage.color = color;
    }

    void UpdateMove()
    {
        if (Phase == WarningUIPhase.End)
        {
            return;
        }

        Vector2 position = TextBackRectTransform.anchoredPosition;

        float now = Time.time;
        switch (Phase)
        {
            case WarningUIPhase.In:
                {
                    CurrentPosX = Mathf.Lerp(CurrentPosX, 0f, (now - MoveStartTime) / MaxMoveTime);
                    position.x = CurrentPosX;
                    TextBackRectTransform.anchoredPosition = position;
                }
                break;

            case WarningUIPhase.Out:
                {
                    CurrentPosX = Mathf.Lerp(CurrentPosX, -CanvaseRectTransform.sizeDelta.x, (now - MoveStartTime) / MaxMoveTime);
                    position.x = CurrentPosX;
                    TextBackRectTransform.anchoredPosition = position;
                }
                break;

            default:
                break;
        }

        switch (Phase)
        {
            case WarningUIPhase.Alert:
                {
                    SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.AlertBoss, soundPriority);
                    Phase = WarningUIPhase.In;
                }
                break;
            case WarningUIPhase.In:
                {
                    if (CurrentPosX < 1.0f)
                    {
                        Phase = WarningUIPhase.Stay;
                        MoveStartTime = now;
                        OnPhaseStay();
                    }
                }
                break;

            case WarningUIPhase.Stay:
                {
                    if (StayTime < (Time.time - MoveStartTime))
                    {
                        Phase = WarningUIPhase.Out;
                        MoveStartTime = now;
                    }
                }
                break;

            case WarningUIPhase.Out:
                {
                    if (CurrentPosX < -CanvaseRectTransform.sizeDelta.x + 1.0f)
                    {
                        Phase = WarningUIPhase.End;
                        OnPhaseEnd();
                    }
                }
                break;

            default:
                break;
        }
    }

    void OnPhaseStay()
    {
        SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().GenerateBoss();
    }

    void OnPhaseEnd()
    {
        Close();

        if (((FWNetworkManager)FWNetworkManager.singleton).isServer)
        {
            SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().SetRunningState();
        }
    }
}
