﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkConfigPanel : BasePanel
{
    const string DefaultIPAddress = "localhost";
    const string DefaultPort = "7777";

    [SerializeField]
    InputField IPAddressInputField;

    [SerializeField]
    InputField PortInputField;

    public override void InitializePanel()
    {
        base.InitializePanel();

        IPAddressInputField.text = DefaultIPAddress;
        PortInputField.text = DefaultPort;
        Close();
    }

    public override void Show()
    {
        base.Show();

        RectTransform rect = gameObject.GetComponent<RectTransform>();
        rect.anchoredPosition = new Vector2(0, rect.anchoredPosition.y);
    }

    public void OnHostButton()
    {
        SystemManager.Instance.ConnectionInfo.Host = true;
        TitleSceneMain sceneMain = SystemManager.Instance.GetCurrentSceneMain<TitleSceneMain>();
        sceneMain.GotoNextScene();
    }

    public void OnClientButton()
    {
        if (string.IsNullOrEmpty(IPAddressInputField.text) == false || IPAddressInputField.text != DefaultIPAddress)
        {
            SystemManager.Instance.ConnectionInfo.IPAddress = IPAddressInputField.text;
        }

        if (string.IsNullOrEmpty(PortInputField.text) == false || PortInputField.text != DefaultPort)
        {
            int port = 0;
            if (int.TryParse(PortInputField.text, out port))
            {
                SystemManager.Instance.ConnectionInfo.Port = port;
            }
            else
            {
                Debug.LogError("OnClientButton error, port : " + port);
                return;
            }
        }

        SystemManager.Instance.ConnectionInfo.Host = false;

        TitleSceneMain sceneMain = SystemManager.Instance.GetCurrentSceneMain<TitleSceneMain>();

        sceneMain.GotoNextScene();
    }
}
