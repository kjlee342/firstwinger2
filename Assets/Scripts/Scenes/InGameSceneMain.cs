﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class InGameSceneMain : BaseSceneMain
{
    public GameState CurrentGameState
    {
        get
        {
            return NetworkTransfer.CurrentGameState;
        }
    }

    List<Player> players = null;
    public List<Player> Players
    {
        get
        {
            if (players == null)
            {
                players = new List<Player>() { Hero, OtherPlayer };
            }

            return players;
        }
    }

    [SerializeField]
    Player player;
    public Player Hero
    {
        get
        {
            return player;
        }

        set
        {
            player = value;
        }
    }

    Player otherPlayer;
    public Player OtherPlayer
    {
        get
        {
            return otherPlayer;
        }

        set
        {
            otherPlayer = value;
        }
    }

    GamePointAccumulator gamePoint = new GamePointAccumulator();
    public GamePointAccumulator GamePoint
    {
        get
        {
            return gamePoint;
        }
    }

    [SerializeField]
    EffectManager effectManager;
    public EffectManager EffectManager
    {
        get
        {
            return effectManager;
        }
    }

    [SerializeField]
    EnemyManager enemyManager;
    public EnemyManager EnemyManager
    {
        get
        {
            return enemyManager;
        }
    }

    [SerializeField]
    BulletManager bulletManager;
    public BulletManager BulletManager
    {
        get
        {
            return bulletManager;
        }
    }

    [SerializeField]
    DamageManager damageManager;
    public DamageManager DamageManager
    {
        get
        {
            return damageManager;
        }
    }

    [SerializeField]
    SquadronManager squadronManager;
    public SquadronManager SquadronManager
    {
        get
        {
            return squadronManager;
        }
    }

    [SerializeField]
    ItemBoxManager itemBoxManager;
    public ItemBoxManager ItemBoxManager
    {
        get
        {
            return itemBoxManager;
        }
    }

    [SerializeField]
    AddonManager addonManager;
    public AddonManager AddonManager
    {
        get
        {
            return addonManager;
        }
    }

    PrefabCacheSystem enemyCacheSystem = new PrefabCacheSystem();
    public PrefabCacheSystem EnemyCacheSystem
    {
        get
        {
            return enemyCacheSystem;
        }
    }

    PrefabCacheSystem bulletCacheSystem = new PrefabCacheSystem();
    public PrefabCacheSystem BulletCacheSystem
    {
        get
        {
            return bulletCacheSystem;
        }
    }

    PrefabCacheSystem effectCacheSystem = new PrefabCacheSystem();
    public PrefabCacheSystem EffectCacheSystem
    {
        get
        {
            return effectCacheSystem;
        }
    }

    PrefabCacheSystem damageCacheSystem = new PrefabCacheSystem();
    public PrefabCacheSystem DamageCacheSystem
    {
        get
        {
            return damageCacheSystem;
        }
    }

    PrefabCacheSystem itemBoxCacheSystem = new PrefabCacheSystem();
    public PrefabCacheSystem ItemBoxCacheSystem
    {
        get
        {
            return itemBoxCacheSystem;
        }
    }

    PrefabCacheSystem addonCacheSystem = new PrefabCacheSystem();
    public PrefabCacheSystem AddonCacheSystem
    {
        get
        {
            return addonCacheSystem;
        }
    }

    [SerializeField]
    Transform mainBGQuadTransform;
    public Transform MainBGQuadTransform
    {
        get
        {
            return mainBGQuadTransform;
        }
    }

    [SerializeField]
    InGameNetworkTransfer inGameNetworkTransfer;
    InGameNetworkTransfer NetworkTransfer
    {
        get
        {
            return inGameNetworkTransfer;
        }
    }

    ActorManager actorManager = new ActorManager();
    public ActorManager ActorManager
    {
        get
        {
            return actorManager;
        }
    }

    [SerializeField]
    Transform player1StartPosition;
    public Vector3 Player1StartPosition
    {
        get
        {
            return player1StartPosition.position;
        }

    }

    [SerializeField]
    Transform player2StartPosition;
    public Vector3 Player2StartPosition
    {
        get
        {
            return player2StartPosition.position;
        }

    }

    [SerializeField]
    int BossEnemyID;

    [SerializeField]
    Vector3 BossGeneratePosition;

    [SerializeField]
    Vector3 BossAppearPosition;

    protected override void UpdateScene()
    {
        base.UpdateScene();

        if (CurrentGameState == GameState.Running)
        {
            int playerCount = 1;
            int playerDeadCount = 0;
            if (player != null)
            {
                playerCount += player.Life;
                if (player.IsDead)
                {
                    playerDeadCount++;
                }
            }

            if (otherPlayer != null)
            {
                playerCount += otherPlayer.Life;
                playerCount++;
                if (otherPlayer.IsDead)
                {
                    playerDeadCount++;
                }
            }

            if (playerCount == playerDeadCount)
            {
                NetworkTransfer.SetGameStateEnd();
                OnGameEnd(false);
            }
        }
    }

    public void GameStart()
    {
        inGameNetworkTransfer.RpcGameStart();
    }

    public void ShowWarningUI()
    {
        NetworkTransfer.RpcShowWarningUI();
    }

    public void SetRunningState()
    {
        NetworkTransfer.RpcSetRunningState();
    }

    public void GenerateBoss()
    {
        SquadronMemberStruct data = new SquadronMemberStruct();
        data.EnemyID = BossEnemyID;
        data.GeneratePointX = BossGeneratePosition.x;
        data.GeneratePointY = BossGeneratePosition.y;
        data.AppearPointX = BossAppearPosition.x;
        data.AppearPointY = BossAppearPosition.y;
        data.DisappearPointX = -15.0f;
        data.DisappearPointY = 0f;

        EnemyManager.GenerateEnemy(data);
    }

    public void OnGameEnd(bool success)
    {
        if (((FWNetworkManager)FWNetworkManager.singleton).isServer)
        {
            NetworkTransfer.RpcGameEnd(success);
        }
    }

    public void GotoTitleScene()
    {
        FWNetworkManager.Shutdown();
        DestroyImmediate(SystemManager.Instance.gameObject);
        SceneController.Instance.LoadSceneImmediate(SceneNameConstants.TitleScene);
    }
}
