﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleSceneMain : BaseSceneMain
{
    protected override void OnStart()
    {
        FWNetworkManager[] FWNetworkManagers = GameObject.FindObjectsOfType<FWNetworkManager>();
        if (FWNetworkManagers != null)
        {
            for (int i = 0; i < FWNetworkManagers.Length; i++)
            {
                DestroyImmediate(FWNetworkManagers[i].gameObject);
            }
        }
    }

    public void OnStartButton()
    {
        PanelManager.GetPanel(typeof(NetworkConfigPanel)).Show();
    }

    public void GotoNextScene()
    {
        SceneController.Instance.LoadScene(SceneNameConstants.LoadingScene);
    }
}
