﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingSceneMain : BaseSceneMain
{
    const float NextSceneInterval = 0.5f;
    const float TextUpdateinterval = 0.15f;
    const string LoadingTextValue = "Loading...";

    [SerializeField]
    Text LoadingText;

    int TextIndex = 0;
    float LastUpdateTime = 0.0f;

    float SceneStartTime = 0.0f;
    bool NextSceneCall = false;

    protected override void OnStart()
    {
        SceneStartTime = Time.time;
    }

    protected override void UpdateScene()
    {
        base.UpdateScene();

        float currentTime = Time.time;
        if (TextUpdateinterval < currentTime - LastUpdateTime)
        {
            LoadingText.text = LoadingTextValue.Substring(0, TextIndex + 1);
            TextIndex++;

            if (LoadingTextValue.Length <= TextIndex)
            {
                TextIndex = 0;
            }

            LastUpdateTime = currentTime;
        }

        if (NextSceneInterval < currentTime - SceneStartTime)
        {
            if (NextSceneCall == false)
            {
                GotoNextScene();
            }
        }
    }

    void GotoNextScene()
    {
        //SceneController.Instance.LoadScene(SceneNameConstants.InGameScene);

        NetworkConnectionInfo info = SystemManager.Instance.ConnectionInfo;

        if (info.Host)
        {
            FWNetworkManager.singleton.StartHost();
            NextSceneCall = true;
        }
        else
        {
            if (string.IsNullOrEmpty(info.IPAddress))
            {
                FWNetworkManager.singleton.networkAddress = info.IPAddress;
            }

            if (info.Port != FWNetworkManager.singleton.networkPort)
            {
                FWNetworkManager.singleton.networkPort = info.Port;
            }

            FWNetworkManager.singleton.StartClient();
            NextSceneCall = true;
        }
    }
}
