﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Bomb : Bullet
{
    //const float MaxRotateTime = 30.0f;

    //const float MaxRotateZ = 90.0f;

    [SerializeField]
    Rigidbody SelfRigidbody;

    [SerializeField]
    Vector3 Force;

    //[SyncVar]
    //float RotateStartTime = 0.0f;

    [SerializeField]
    [SyncVar]
    float CurrentRotateZ;

    //Vector3 currentEulerAngles = Vector3.zero;

    [SerializeField]
    SphereCollider ExplodeArea;

    const int BombSoundPriority = 6;

    protected override void UpdateTransform()
    {
        if (NeedMove == false)
        {
            return;
        }

        UpdateRotate();
    }

    protected override void Disapper()
    {
        SelfRigidbody.useGravity = false;
        SelfRigidbody.velocity = Vector3.zero;
        NeedMove = false;

        Explode();
    }

    void UpdateRotate()
    {
        //CurrentRotateZ = Mathf.Lerp(CurrentRotateZ, MaxRotateZ, (Time.time - RotateStartTime) / MaxRotateTime);
        //currentEulerAngles.z = -CurrentRotateZ;

        //Quaternion rot = Quaternion.identity;
        //rot.eulerAngles = currentEulerAngles;
        //transform.localRotation = rot;

        transform.right = SelfRigidbody.velocity.normalized;
    }

    public override void Fire(int ownerInstanceID, Vector3 direction, float speed, int damage)
    {
        base.Fire(ownerInstanceID, direction, speed, damage);

        AddForce(Force);
    }

    void InternalAddForce(Vector3 force)
    {
        transform.right = Vector3.right;
        SelfRigidbody.velocity = Vector3.zero;

        SelfRigidbody.useGravity = true;
        SelfRigidbody.AddForce(force, ForceMode.Impulse);

        ExplodeArea.enabled = false;

        //RotateStartTime = Time.time;
        //CurrentRotateZ = 0.0f;
        //transform.localRotation = Quaternion.identity;
    }

    public void AddForce(Vector3 force)
    {
        //CmdAddForce(force);
        if (isServer)
        {
            RpcAddForce(force);
        }
        else
        {
            CmdAddForce(force);
            if (isLocalPlayer)
            {
                InternalAddForce(force);
            }
        }
    }

    [Command]
    void CmdAddForce(Vector3 force)
    {
        InternalAddForce(force);
        base.SetDirtyBit(1);
    }

    [ClientRpc]
    void RpcAddForce(Vector3 force)
    {
        InternalAddForce(force);
        base.SetDirtyBit(1);
    }

    void InternalExplode()
    {
        InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();
        inGameSceneMain.EffectManager.GenerateEffect(EffectManager.BombExplodeFxIndex, transform.position);

        SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.ExplodePlayerBomb, BombSoundPriority);

        ExplodeArea.enabled = true;

        List<Enemy> targetList = inGameSceneMain.EnemyManager.GetContainEnemies(ExplodeArea);
        for (int i = 0; i < targetList.Count; i++)
        {
            if (targetList[i].IsDead)
            {
                continue;
            }

            targetList[i].OnBulletHited(Damage, targetList[i].transform.position);
        }

        if (gameObject.activeSelf)
        {
            base.Disapper();
        }
    }

    void Explode()
    {
        //CmdExplode();

        if (isServer)
        {
            RpcExplode();
        }
        else
        {
            CmdExplode();
            if (isLocalPlayer)
            {
                InternalExplode();
            }
        }
    }

    [Command]
    public void CmdExplode()
    {
        InternalExplode();
        base.SetDirtyBit(1);
    }

    [ClientRpc]
    public void RpcExplode()
    {
        InternalExplode();
        base.SetDirtyBit(1);
    }

    protected override bool OnBullteConllision(Collider collider)
    {
        if (base.OnBullteConllision(collider) == false)
        {
            return false;
        }

        Explode();

        return true;
    }
}
