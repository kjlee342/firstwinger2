﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Beam : MonoBehaviour
{
    public const string FilePath = "Prefabs/Beam";
    
    const float PreheatTimeCriterion = 1.0f;
    const float WarningTimeCriterion = 0.2f;

    const float HoldingTimeCriterion = 3.0f;
    const float HitCheckTimeCriterion = 0.1f;

    const int BeamSoundPrioiry = 5;

    public enum BeamState : int
    {
        None = 0,
        Ready,
        Fire,
        End,
    }

    BeamState state = BeamState.None;
    public BeamState State
    {
        get
        {
            return state;
        }
    }

    [SerializeField]
    int damage = 100;

    [SerializeField]
    GameObject Area;

    Collider areaCollider = null;
    MeshRenderer areaMesh = null;


    [SerializeField]
    GameObject Effect;

    ParticleSystem particle;

    [SerializeField]
    bool WarningBeforFIre = true;

    bool warningToggle = false;

    float ActionTime = 0f;
    float subActionTime = 0f;

    void Update()
    {
        switch (state)
        {
            case BeamState.Ready:
                Ready();
                break;

            case BeamState.Fire:
                InternalFire();
                break;

            case BeamState.End:
            case BeamState.None:
            default:
                break;
        }
    }

    public void Initialize()
    {
        areaCollider = Area.GetComponentInChildren<Collider>();
        areaMesh = Area.GetComponentInChildren<MeshRenderer>();
        particle = Effect.GetComponentInChildren<ParticleSystem>();

        areaCollider.enabled = false;
        areaMesh.enabled = false;
        particle.Stop();

        state = BeamState.None;
    }

    public void Fire(bool warningBeforFIre = true)
    {
        state = BeamState.Ready;
        WarningBeforFIre = warningBeforFIre;
        ActionTime = Time.time;
        subActionTime = Time.time;
        warningToggle = false;
        SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.ShotBossBeam, BeamSoundPrioiry);
    }

    void Ready()
    {
        float now = Time.time;
        if (WarningBeforFIre)
        {
            if (PreheatTimeCriterion < now - ActionTime)
            {
                state = BeamState.Fire;
                ActionTime = now;
                Effect.SetActive(true);
                particle.Play();
                areaMesh.enabled = false;

            }
            else if (WarningTimeCriterion < now - subActionTime)
            {
                areaMesh.enabled = warningToggle;
                warningToggle = !warningToggle;
                subActionTime = now;
            }
        }
        else
        {
            areaMesh.enabled = false;
            state = BeamState.Fire;
        }
    }

    bool CheckBoundary(Actor actor)
    {
        Collider playerCollider = actor.GetComponentInChildren<Collider>();

        return areaCollider.bounds.Intersects(playerCollider.bounds);
    }

    private void InternalFire()
    {
        float now = Time.time;

        if (HitCheckTimeCriterion < now - subActionTime)
        {
            areaCollider.enabled = true;

            InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();

            List<Player> playerList = inGameSceneMain.Players;
            for (int i = 0; i < playerList.Count; i++)
            {
                if (playerList[i] == null || playerList[i].IsDead)
                {
                    continue;
                }

                if (CheckBoundary(playerList[i]))
                {
                    playerList[i].OnBulletHited(damage, playerList[i].transform.position);
                }
            }

            subActionTime = now;
            areaCollider.enabled = false;
        }

        if (HoldingTimeCriterion < now - ActionTime)
        {
            areaCollider.enabled = false;
            particle.Stop();

            Effect.SetActive(false);
            state = BeamState.End;
        }
    }

    public void End()
    {
        state = BeamState.None;
    }
}
