﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class Bullet : NetworkBehaviour
{
    const float LifeTime = 10f;

    [SerializeField]
    [SyncVar]
    int OnwerInstanceID;

    [SerializeField]
    [SyncVar]
    protected Vector3 MoveDirection = Vector3.zero;

    [SerializeField]
    [SyncVar]
    protected float Speed = 0f;

    [SyncVar]
    protected bool NeedMove = false;

    [SyncVar]
    bool Hited = false;

    [SerializeField]
    [SyncVar]
    protected int Damage = 1;

    protected float FiredTime = 0f;

    [SerializeField]
    [SyncVar]
    string filePath;

    public string FilePath
    {
        get
        {
            return filePath;
        }

        set
        {
            filePath = value;
        }
    }

    protected SoundEffectList CollisionEffectSound = SoundEffectList.ImpactPlayerBullet;

    private void Start()
    {
        if (((FWNetworkManager)FWNetworkManager.singleton).isServer == false)
        {
            InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();
            transform.SetParent(inGameSceneMain.BulletManager.transform);
            inGameSceneMain.BulletCacheSystem.Add(filePath, gameObject);
            gameObject.SetActive(false);
        }
    }

    void Update()
    {
        if (ProcessDisapperCondition())
        {
            Disapper();
            return;
        }

        UpdateTransform();
    }

    protected virtual void UpdateTransform()
    {
        UpdateMove();
    }

    private bool ProcessDisapperCondition()
    {
        Transform mainBGQuadTransform = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().MainBGQuadTransform;

        float bottom = -mainBGQuadTransform.localScale.y * 0.5f;
        float top = mainBGQuadTransform.localScale.y;
        float side = mainBGQuadTransform.localScale.x * 0.5f;

        if (side < Mathf.Abs(transform.position.x) || top < transform.position.y || transform.position.y < bottom)
        {
            return true;
        }
        else if (LifeTime < Time.time - FiredTime)
        {
            return true;
        }

        return false;
    }

    protected virtual void Disapper()
    {
        SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().BulletManager.Remove(this);
    }

    protected virtual void UpdateMove()
    {
        if (NeedMove == false)
        {
            return;
        }

        Vector3 moveVector = MoveDirection.normalized * Speed * Time.deltaTime;
        moveVector = AdjustMove(moveVector);
        transform.position += moveVector;
    }

    public void InternelFire(int onwerInstanceID, Vector3 direction, float speed, int damage)
    {
        OnwerInstanceID = onwerInstanceID;
        MoveDirection = direction;
        Speed = speed;
        Damage = damage;
        FiredTime = Time.time;
        NeedMove = true;
        Hited = false;
    }

    public virtual void Fire(int ownerInstanceID, Vector3 direction, float speed, int damage)
    {
        //CmdFire(ownerInstanceID, direction, speed, damage);

        if (isServer)
        {
            RpcFire(ownerInstanceID, direction, speed, damage);
        }
        else
        {
            CmdFire(ownerInstanceID, direction, speed, damage);
            if (isLocalPlayer)
            {
                InternelFire(ownerInstanceID, direction, speed, damage);
            }
        }
    }

    [Command]
    public void CmdFire(int ownerInstanceID, Vector3 direction, float speed, int damage)
    {
        InternelFire(ownerInstanceID, direction, speed, damage);
        base.SetDirtyBit(1);
    }

    [ClientRpc]
    public void RpcFire(int ownerInstanceID, Vector3 direction, float speed, int damage)
    {
        InternelFire(ownerInstanceID, direction, speed, damage);
        base.SetDirtyBit(1);
    }

    protected Vector3 AdjustMove(Vector3 moveVector)
    {
        RaycastHit hitInfo;
        if (Physics.Linecast(transform.position, transform.position + moveVector, out hitInfo))
        {
            int colliderLayer = hitInfo.collider.gameObject.layer;
            if (colliderLayer != LayerMask.NameToLayer("Enemy") && colliderLayer != LayerMask.NameToLayer("Player"))
            {
                return moveVector;
            }

            Actor actor = hitInfo.collider.GetComponentInParent<Actor>();
            if (actor && actor.IsDead)
            {
                return moveVector;
            }

            moveVector = hitInfo.point - transform.position;
            OnBullteConllision(hitInfo.collider);
        }

        return moveVector;
    }

    protected virtual bool OnBullteConllision(Collider collider)
    {
        if (Hited)
        {
            return false;
        }

        int EnemyBulletIndex = LayerMask.NameToLayer("EnemyBullet");
        int PlayerBulletIndex = LayerMask.NameToLayer("PlayerBullet");
        if (collider.gameObject.layer == EnemyBulletIndex
            || collider.gameObject.layer == PlayerBulletIndex)
        {
            return false;
        }

        Actor owner = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().ActorManager.GetActor(OnwerInstanceID);
        if (owner == null)
        {
            //Debug.Log("owner == null");
            return false;
        }

        Actor actor = collider.GetComponentInParent<Actor>();
        if (actor == null)
        {
            //Debug.Log("actor == null");
            return false;
        }

        if (actor.IsDead || actor.ActorInstanceID == owner.ActorInstanceID)
        {
            //Debug.Log("actor.IsDead || actor.ActorInstanceID == owner.ActorInstanceID");
            return false;
        }

        actor.OnBulletHited(Damage, transform.position);

        SystemManager.Instance.SoundManager.PlayEffect(CollisionEffectSound);
        
        Hited = true;
        NeedMove = false;

        GameObject go = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().EffectManager.GenerateEffect(EffectManager.BulletDisapperFxIndex, transform.position);
        if (go != null)
        {
            go.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        }

        Disapper();

        return true;
    }

    private void OnTriggerEnter(Collider other)
    {
        int colliderLayer = other.gameObject.layer;
        if (colliderLayer != LayerMask.NameToLayer("Enemy") && colliderLayer != LayerMask.NameToLayer("Player"))
        {
            return;
        }

        OnBullteConllision(other);
    }

    [ClientRpc]
    public void RpcSetActive(bool value)
    {
        this.gameObject.SetActive(value);
        base.SetDirtyBit(1);
    }

    public void SetPosition(Vector3 position)
    {
        //CmdSetPosition(position);

        if (isServer)
        {
            RpcSetPosition(position);
        }
        else
        {
            CmdSetPosition(position);
            if (isLocalPlayer)
            {
                transform.position = position;
            }
        }
    }

    [Command]
    public void CmdSetPosition(Vector3 position)
    {
        this.transform.position = position;
        base.SetDirtyBit(1);
    }

    [ClientRpc]
    public void RpcSetPosition(Vector3 position)
    {
        this.transform.position = position;
        base.SetDirtyBit(1);
    }

}
