﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerAddon : NetworkBehaviour
{
    const float FireInterval = 0.1f;

    const float MinimumDistance = 1f;
    const float AccurateModeMinimumDistance = 0.01f;

    [SerializeField]
    int damage = 50;
    public int Damage
    {
        get
        {
            return damage;
        }
    }

    [SerializeField]
    [SyncVar]
    int OnwerInstanceID;

    [SerializeField]
    float Speed = 15f;
    
    [SerializeField]
    [SyncVar]
    Vector3 CurrentVelocity = Vector3.zero;

    [SerializeField]
    float bulletSpeed = 10f;
    
    public float BulletSpeed
    {
        get
        {
            return bulletSpeed;
        }
    }

    [SyncVar]
    float FiredTime = 0f;

    [SerializeField]
    Transform fireTransform;
    public Transform FireTransform
    {
        get
        {
            return fireTransform;
        }

    }

    [SerializeField]
    [SyncVar]
    string filePath;

    [SerializeField]
    Transform MoveTarget = null;

    [SerializeField]
    [SyncVar]
    bool accurateMode = false;

    public bool AccurateMode
    {
        get
        {
            return accurateMode;
        }

        set
        {
            accurateMode = value;
        }
    }

    [SerializeField]
    Transform AccurateModePosition = null;

    public string FilePath
    {
        get
        {
            return filePath;
        }

        set
        {
            filePath = value;
        }
    }

    private void Start()
    {
        if (((FWNetworkManager)FWNetworkManager.singleton).isServer == false)
        {
            InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();
            transform.SetParent(inGameSceneMain.AddonManager.transform);
            inGameSceneMain.AddonCacheSystem.Add(filePath, gameObject);
            gameObject.SetActive(false);
        }
    }

    void Update()
    {
        UpdateMove();
    }

    void UpdateMove()
    {
        Vector3 moveVector = MoveTarget.transform.position;
        float CheckDistance = MinimumDistance;

        if (accurateMode)
        {
            moveVector = AccurateModePosition.position;
            CheckDistance = AccurateModeMinimumDistance;
        }

        float distance = Vector3.Distance(moveVector, transform.position);
        if (distance <= CheckDistance)
        {
            return;

        }

        transform.position = Vector3.SmoothDamp(transform.position, moveVector, ref CurrentVelocity, 0.1f);
    }

    public void Disapper()
    {
        SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().AddonManager.Remove(this);
    }

    public void InitializeAddon(Transform moveTarget, Transform accurateModePosition)
    {
        MoveTarget = moveTarget;
        AccurateModePosition = accurateModePosition;
    }

    public bool Fire()
    {
        float now = Time.time;
        if (now - FiredTime < FireInterval)
        {
            return false;
        }

        FiredTime = now;

        SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.ShotPlayerBullet);

        return true;
    }

    [ClientRpc]
    public void RpcSetActive(bool value)
    {
        this.gameObject.SetActive(value);
        base.SetDirtyBit(1);
    }

    [ClientRpc]
    public void RpcSetPosition(Vector3 position)
    {
        this.transform.position = position;
        base.SetDirtyBit(1);
    }
}
