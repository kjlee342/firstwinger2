﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GuidedMissile : Bullet
{
    const float ChaseFactor = 1.5f;

    const float ChasingStartTime = 0.7f;

    const float ChasingEndTime = 5.5f;

    [SerializeField]
    [SyncVar]
    int TargetInstanceID;

    [SerializeField]
    Vector3 ChaseVector;

    [SerializeField]
    Vector3 rotateVector = Vector3.zero;

    [SerializeField]
    bool FlipDirection = true;

    public void FireChase(int targetInstanceID, int ownerInstaceID, Vector3 direction, float speed, int damage)
    {
        if (isServer == false)
        {
            return;
        }

        RpcSetTargetInstanceID(targetInstanceID);
        base.Fire(ownerInstaceID, direction, speed, damage);
    }

    [ClientRpc]
    public void RpcSetTargetInstanceID(int targetInstanceID)
    {
        TargetInstanceID = targetInstanceID;
        base.SetDirtyBit(1);
    }

    protected override void UpdateTransform()
    {
        UpdateMove();
        UpdateRotate();
    }

    protected override void UpdateMove()
    {
        if (NeedMove == false)
        {
            return;
        }

        Vector3 moveVector = MoveDirection.normalized * Speed * Time.deltaTime;
        float delthTime = Time.time - FiredTime;

        if (ChasingStartTime < delthTime && delthTime < ChasingEndTime)
        {
            Actor target = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().ActorManager.GetActor(TargetInstanceID);
            if (target != null)
            {
                Vector3 targetVector = target.transform.position - transform.position;

                ChaseVector = Vector3.Lerp(moveVector.normalized, targetVector.normalized, ChaseFactor * Time.deltaTime);

                moveVector += ChaseVector.normalized;
                moveVector = moveVector.normalized * Speed * Time.deltaTime;

                MoveDirection = moveVector.normalized;
            }
        }

        moveVector = AdjustMove(moveVector);
        transform.position += moveVector;

        rotateVector.z = Vector2.SignedAngle(Vector2.right, moveVector);
        if (FlipDirection)
        {
            rotateVector.z += 180f;
        }
    }

    void UpdateRotate()
    {
        Quaternion quat = Quaternion.identity;
        quat.eulerAngles = rotateVector;
        transform.rotation = quat;
    }

    protected override bool OnBullteConllision(Collider collider)
    {
           
        if (base.OnBullteConllision(collider))
        {
            SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.ExplodeBossMissile);

            return true;
        }

        return false;
    }
}
