﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

public class Player : Actor
{
    const string PlayerHUDPath = "Prefabs/PlayerHUD";
    const int MaximumAddon = 4;
    const int MinimumSpreadItem = 2;

    const int BombSoundPrioiry = 4;
    const int PlayerDeadSoundPriority = 6;

    [SerializeField]
    [SyncVar]
    Vector3 MoveVector = Vector3.zero;

    [SerializeField]
    NetworkIdentity networkIdentity = null;

    public NetworkIdentity NetworkIdentity
    {
        get
        {
            return networkIdentity;
        }
    }

    [SerializeField]
    float Speed = 5;

    [SerializeField]
    BoxCollider boxCollider;

    [SerializeField]
    Transform FireTransform;

    [SerializeField]
    float FireInterval = 0.1f;
    float FiredTime = 0f;

    [SerializeField]
    float BulletSpeed = 10f;

    InputController inputController = new InputController();

    [SerializeField]
    [SyncVar]
    bool Host = false;

    [SerializeField]
    Material ClientPlayerMaterial;

    [SerializeField]
    [SyncVar]
    int UsableItemCount = 0;

    public int ItemCount
    {
        get
        {
            return UsableItemCount;
        }
    }

    [SerializeField]
    GameObject AccurateVisibility;

    [SerializeField]
    SphereCollider AccurateCollider;

    [SerializeField]
    [SyncVar]
    bool accurateMode = false;

    public bool AccurateMode
    {
        get
        {
            return accurateMode;
        }

        set
        {
            accurateMode = value;
            SetAccurateMode();
        }
    }

    [SerializeField]
    Transform[] AccurateModeAddonPositions;

    [SerializeField]
    List<(GameObject, PlayerAddon)> addons = new List<(GameObject, PlayerAddon)>();

    [SerializeField]
    [SyncVar]
    int life = 2;

    public int Life
    {
        get
        {
            return life;
        }

    }

    protected override void Initialize()
    {
        base.Initialize();

        soundPrioiry = 1;

        InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();

        if (isLocalPlayer)
        {
            inGameSceneMain.Hero = this;
        }
        else
        {
            inGameSceneMain.OtherPlayer = this;
        }

        if (isServer && isLocalPlayer)
        {
            Host = true;
            RpcSetHost();
        }

        Transform startTransform;
        if (Host == false)
        {
            MeshRenderer meshRenderer = GetComponentInChildren<MeshRenderer>();
            meshRenderer.material = ClientPlayerMaterial;
        }

        if (actorInstanceID != 0)
        {
            inGameSceneMain.ActorManager.Regist(actorInstanceID, this);
        }

        InitializePlayerHUD();
    }

    void InitializePlayerHUD()
    {
        InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();
        GameObject go = Resources.Load<GameObject>(PlayerHUDPath);
        GameObject goInstance = Instantiate<GameObject>(go, Camera.main.WorldToScreenPoint(transform.position), Quaternion.identity, inGameSceneMain.DamageManager.CanvasTransform);
        playerHUD = goInstance.GetComponent<PlayerHUD>();
        playerHUD.Initialize(this);
    }

    protected override void UpdateActor()
    {
        if (isLocalPlayer == false)
        {
            return;
        }

        UpdateInput();
        UpdateMove();
    }

    [ClientCallback]
    public void UpdateInput()
    {
        inputController.UpdateInput();
    }

    void UpdateMove()
    {
        if (MoveVector.sqrMagnitude == 0 || isDead)
        {
            return;
        }

        if (isServer)
        {
            RpcMove(AdjustMoveVector(MoveVector));
        }
        else
        {
            CmdMove(AdjustMoveVector(MoveVector));
            if (isLocalPlayer)
            {
                transform.position += AdjustMoveVector(MoveVector);
            }
        }
    }

    [Command]
    public void CmdMove(Vector3 moveVector)
    {
        this.MoveVector = moveVector;
        transform.position += moveVector;
        base.SetDirtyBit(1);
        this.MoveVector = Vector3.zero;
    }

    [ClientRpc]
    public void RpcMove(Vector3 moveVector)
    {
        this.MoveVector = moveVector;
        transform.position += moveVector;
        base.SetDirtyBit(1);
        this.MoveVector = Vector3.zero;
    }

    private Vector3 AdjustMoveVector(Vector3 moveVector)
    {
        Transform mainBGQuadTransform = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().MainBGQuadTransform;

        Vector3 result = boxCollider.transform.position + moveVector;
        
        if (result.x - boxCollider.size.x * 0.5f < -mainBGQuadTransform.localScale.x * 0.5f)
        {
            moveVector.x = 0;
        }
        else if (result.x + boxCollider.size.x * 0.5f > mainBGQuadTransform.localScale.x * 0.5f)
        {
            moveVector.x = 0;
        }

        if (result.y - boxCollider.size.y * 0.5f < -mainBGQuadTransform.localScale.y * 0.5f)
        {
            moveVector.y = 0;
        }
        else if (result.y + boxCollider.size.y * 0.5f > mainBGQuadTransform.localScale.y * 0.5f)
        {
            moveVector.y = 0;
        }

        return moveVector;
    }

    public void ProcessInput(Vector3 moveDirection)
    {
        MoveVector = moveDirection * Speed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        Enemy enemy = other.GetComponentInParent<Enemy>();
        if (enemy != null &&  enemy.IsDead == false)
        {
            BoxCollider box = (BoxCollider) other;
            Vector3 crashPosition = enemy.transform.position + box.center;
            crashPosition.x += box.size.x * 0.5f;

            enemy.OnCrash(CrashDamage, crashPosition);
        }
    }

    public void Fire()
    {
        float now = Time.time;
        if (now - FiredTime < FireInterval)
        {
            return;
        }

        if (Host)
        {
            Bullet bullet = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().BulletManager.Generate(BulletManager.PlayerBulletIndex, FireTransform.position);
            bullet.Fire(actorInstanceID, FireTransform.right, BulletSpeed, Damage);

            for (int i = 0; i < addons.Count; i++)
            {
                if (addons[i].Item2.Fire())
                {
                    bullet = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().BulletManager.Generate(BulletManager.PlayerAddonBulletIndex, addons[i].Item2.FireTransform.position);
                    bullet.Fire(actorInstanceID, addons[i].Item2.FireTransform.right, addons[i].Item2.BulletSpeed, addons[i].Item2.Damage);
                }
            }
        }
        else
        {
            CmdFire(actorInstanceID, FireTransform.position, FireTransform.right, BulletSpeed, Damage);
            for (int i = 0; i < addons.Count; i++)
            {
                CmdAddonFire(actorInstanceID, addons[i].Item2.FireTransform.position, addons[i].Item2.FireTransform.right, addons[i].Item2.BulletSpeed, addons[i].Item2.Damage);
            }
        }

        FiredTime = now;

        SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.ShotPlayerBullet, soundPrioiry);
    }

    [Command]
    public void CmdAddonFire(int onwerInstanceID, Vector3 firePosition, Vector3 direction, float speed, int damage)
    {
        Bullet bullet = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().BulletManager.Generate(BulletManager.PlayerAddonBulletIndex, firePosition);
        bullet.Fire(onwerInstanceID, direction, speed, damage);
        base.SetDirtyBit(1);
    }

    [Command]
    public void CmdFire(int onwerInstanceID, Vector3 firePosition, Vector3 direction, float speed, int damage)
    {
        Bullet bullet = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().BulletManager.Generate(BulletManager.PlayerBulletIndex, firePosition);
        bullet.Fire(onwerInstanceID, direction, speed, damage);
        base.SetDirtyBit(1);
    }

    public void FireBomb()
    {
        float now = Time.time;
        if (UsableItemCount <= 0 || (now - FiredTime) < FireInterval)
        {
            return;
        }

        FiredTime = now;

        if (Host)
        {
            Bullet bullet = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().BulletManager.Generate(BulletManager.PlayerBombIndex, FireTransform.position);
            bullet.Fire(actorInstanceID, FireTransform.right, BulletSpeed, Damage);
        }
        else
        {
            CmdFireBomb(actorInstanceID, FireTransform.position, FireTransform.right, BulletSpeed, Damage);
        }

        DecreaseUsableItemCount();
        SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.ShotPlayerBomb, BombSoundPrioiry);
    }

    [Command]
    public void CmdFireBomb(int onwerInstanceID, Vector3 firePosition, Vector3 direction, float speed, int damage)
    {
        Bullet bullet = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().BulletManager.Generate(BulletManager.PlayerBombIndex, firePosition);
        bullet.Fire(onwerInstanceID, direction, speed, damage);
        base.SetDirtyBit(1);
    }

    void DecreaseUsableItemCount()
    {
        //CmdDecreaseUsableItemCount();
        if (isServer)
        {
            RpcDecreaseUsableItemCount();
        }
        else
        {
            CmdDecreaseUsableItemCount();
            if (isLocalPlayer)
            {
                UsableItemCount--;
            }
        }
    }

    [Command]
    public void CmdDecreaseUsableItemCount()
    {
        UsableItemCount--;
        base.SetDirtyBit(1);
    }

    [ClientRpc]
    public void RpcDecreaseUsableItemCount()
    {
        UsableItemCount--;
        base.SetDirtyBit(1);
    }

    protected override void DecreaseHP(int value, Vector3 position)
    {
        base.DecreaseHP(value, position);

        Vector3 damagePoint = position + Random.insideUnitSphere * 0.5f;
        SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().DamageManager.Generate(DamageManager.PlayerDamageIndex, damagePoint, value, Color.red);
    }

    protected override void OnDead()
    {
        base.OnDead();
        SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.DieActor, PlayerDeadSoundPriority);

        accurateMode = false;
        SetAccurateMode();

        int itemCount = Mathf.Max(MinimumSpreadItem, addons.Count - 1);
        for (int i = 0; i < itemCount; i++)
        {
            GenerateItem();
        }

        for (int i = 0; i < addons.Count; i++)
        {
            addons[i].Item2.Disapper();
        }

        addons.Clear();


        if (0 < life)
        {
            Vector3 newPosition = Vector3.zero;

            if (Host)
            {
                newPosition = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().Player1StartPosition;
            }
            else
            {
                newPosition = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().Player2StartPosition;
            }

            newPosition.x -= 5;
            SetPosition(newPosition);

            newPosition.x += 5;
            StartCoroutine(Resurrect(newPosition));
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    [ClientRpc]
    public void RpcSetHost()
    {
        Host = true;
        base.SetDirtyBit(1);
    }

    protected virtual void InternalIncreaseHP(int value)
    {
        if (isDead)
        {
            return;
        }

        CurrentHP = Mathf.Min(CurrentHP + value, MaxHP);
    }

    public virtual void IncreaseHP(int value)
    {
        if (isDead)
        {
            return;
        }

        CmdIncreaseHP(value);
    }

    [Command]
    public void CmdIncreaseHP(int value)
    {
        InternalIncreaseHP(value);
        base.SetDirtyBit(1);
    }

    public virtual void IncreaseUsableItem(int value = 1)
    {
        if (isDead)
        {
            return;
        }

        CmdIncreaseUsableItem(value);
    }

    [Command]
    public void CmdIncreaseUsableItem(int value)
    {
        UsableItemCount += value;
        base.SetDirtyBit(1);
    }

    Transform GetAddonPosition()
    {
        Transform tf = null;
        if (addons.Count == 0)
        {
            tf = transform;
        }
        else
        {
            tf = addons[addons.Count - 1].Item1.transform;
        }

        return tf;
    }

    public void AddAddon()
    {
        if (isDead)
        {
            return;
        }

        if (isServer)
        {
            RpcAddaddon();
        }
        else
        {
            CmdAddAddon();
            if (isLocalPlayer)
            {
                InternalAddAddon();
            }
        }
    }

    [Command]
    public void CmdAddAddon()
    {
        InternalAddAddon();
        base.SetDirtyBit(1);
    }

    [ClientRpc]
    public void RpcAddaddon()
    {
        InternalAddAddon();
        base.SetDirtyBit(1);
    }

    public void InternalAddAddon()
    {
        if (addons.Count == MaximumAddon)
        {
            InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();
            inGameSceneMain.GamePoint.Add(ItemBox.ScoreAddValue);
            return;
        }

        GameObject addon = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().AddonManager.Generate(0, transform.position);

        PlayerAddon playerAddon = addon.GetComponent<PlayerAddon>();
        playerAddon.InitializeAddon(GetAddonPosition(), AccurateModeAddonPositions[addons.Count]);

        addons.Add((addon, playerAddon));
    }

    void SetAccurateMode()
    {
        if (IsDead)
        {
            return;
        }

        AccurateVisibility.SetActive(accurateMode);
        AccurateCollider.enabled = accurateMode;
        boxCollider.enabled = !accurateMode;

        if (accurateMode)
        {
            Speed *= 0.5f;
        }
        else
        {
            Speed *= 2f;
        }

        for (int i = 0; i < addons.Count; i++)
        {
            addons[i].Item2.AccurateMode = accurateMode;
        }
    }

    IEnumerator Resurrect(Vector3 movePosition)
    {
        Transform model = transform.Find("StarSparrow1");
        Transform afterBunnet = transform.Find("AfterBurner");

        yield return new WaitForSeconds(1.0f);

        Vector3 velocity = Vector3.zero;

        bool Toggle = false;
        bool oneShot = true;
        float regenTime = Time.time;
        float nowTime = Time.time;

        while (nowTime - regenTime < 3.0f)
        {
            yield return new WaitForEndOfFrame();

            model.gameObject.SetActive(Toggle);
            afterBunnet.gameObject.SetActive(Toggle);
            Toggle = !Toggle;
            
            if (nowTime - regenTime < 1.5f)
            {
                transform.position = Vector3.SmoothDamp(transform.position, movePosition, ref velocity, 0.25f);
            }
            else if (oneShot)
            {
                CurrentHP = MaxHP;
                life--;
                isDead = false;
                oneShot = false;
            }

            nowTime = Time.time;
        }

        if (Toggle)
        {
            model.gameObject.SetActive(true);
            afterBunnet.gameObject.SetActive(true);
        }
    }

    void GenerateItem()
    {
        if (isServer == false)
        {
            return;
        }

        Vector3 randomPosition = Random.insideUnitCircle;
        InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();
        inGameSceneMain.ItemBoxManager.Generate(1, transform.position + randomPosition);
    }
}
