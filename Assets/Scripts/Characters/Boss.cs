﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Boss : Enemy
{
    const string BossHUDPath = "Prefabs/BossHUD";
    const int NumberOfBulletOneFire = 55;
    const float BossBulletSpeed = 6.7f;

    const float FireTransformRotationStart = 25.0f;
    const float FireTransformRotationEnd = 355.0f;
    const float FireTransformRotationInterval = 6.1f;
    const float ActionUpdateInternal = 0.3f;

    const int NumberOfBulletFire = 20;

    const int SoundPriority = 5;

    enum SpecialAttackMode : int
    {
        Missile = 0,
        Beam,
    }

    [SyncVar]
    bool needBattleMove = false;

    [SerializeField]
    float BattleMoveMax;

    Vector3 BattleMoveStartPosition;

    int FireRemainCountPerOneTime;

    [SerializeField]
    Vector3 CurrentFireTransformRotation;

    protected override int BulletIndex
    {
        get
        {
            return BulletManager.BossBulletIndex;
        }
    }

    [SerializeField]
    Transform[] MissileFireTransforms;

    Player[] players;

    Player[] Players
    {
        get
        {
            if (players == null)
            {
                players = GameObject.FindObjectsOfType<Player>();
            }

            return players;
        }
    }

    [SyncVar]
    SpecialAttackMode specialAttackMode = SpecialAttackMode.Beam;

    int SpecialAttackCount = 0;

    [SerializeField]
    float MissileSpeed = 2f;

    [SerializeField]
    Beam beam;

    protected override void SetBattleState()
    {
        base.SetBattleState();

        soundPrioiry = SoundPriority;

        BulletSpeed = BossBulletSpeed;

        FireRemainCount = NumberOfBulletFire;
        SpecialAttackCount = MissileFireTransforms.Length;

        BattleMoveStartPosition = transform.position;
        FireRemainCountPerOneTime = FireRemainCount;

        if (playerHUD == null)
        {
            InitializeHUD();
        }

        InitializeBeam();

        CurrentFireTransformRotation.z = FireTransformRotationStart;
        Quaternion quat = Quaternion.identity;
        quat.eulerAngles = CurrentFireTransformRotation;
        FireTransform.localRotation = quat;

    }

    protected override void UpdateBattle()
    {
        if (needBattleMove)
        {
            UpdateBattleMove();
        }
        else
        {
            float now = Time.time;
            if (ActionUpdateInternal < now - ActionUpdateTime)
            {
                if (0 < FireRemainCountPerOneTime || 0 < SpecialAttackCount)
                {
                    SpecialAttack();
                    Fire();

                    if (isServer)
                    {
                        RpcSetDirtyBit();
                    }

                    SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.ShotBossBullet, soundPrioiry);

                    FireRemainCountPerOneTime--;
                }
                else
                {
                    SetBattleMove();
                    SetSpecialAttack();
                }

                ActionUpdateTime = now;
            }
        }
    }

    void SpecialAttack()
    {
        if (SpecialAttackCount <= 0)
        {
            return;
        }

        switch (specialAttackMode)
        {
            case SpecialAttackMode.Missile:
                {
                    FireChase();
                    SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.ShotBossMissile, soundPrioiry);
                    SpecialAttackCount--;
                }
                break;

            case SpecialAttackMode.Beam:
                {
                    if (beam.State == Beam.BeamState.None)
                    {
                        beam.Fire(true);
                    }
                    else if (beam.State == Beam.BeamState.End)
                    {
                        SpecialAttackCount--;
                    }

                    if (SpecialAttackCount == 0)
                    {
                        beam.End();
                    }
                }
                break;

            default:
                break;
        }

    }

    public void Fire()
    {
        Quaternion quat = Quaternion.identity;

        CurrentFireTransformRotation.z = FireTransformRotationStart;

        for (int i = 0; i < NumberOfBulletOneFire; i++)
        {
            CurrentFireTransformRotation.z += FireTransformRotationInterval;
            if (FireTransformRotationEnd <= CurrentFireTransformRotation.z)
            {
                CurrentFireTransformRotation.z -= (FireTransformRotationEnd + FireTransformRotationStart);
            }

            quat.eulerAngles = CurrentFireTransformRotation;
            FireTransform.localRotation = quat;

            base.Fire(FireTransform.right);
        }
    }

    void SetSpecialAttack()
    {
        if (isServer == false)
        {
            return;
        }

        float rand = Random.value;
        SpecialAttackMode mode;

        if (rand < 0.5f)
        {
            mode = SpecialAttackMode.Missile;
        }
        else
        {
            mode = SpecialAttackMode.Beam;
        }

        RpcSetSpecialAttack(mode);
    }

    [ClientRpc]
    void RpcSetSpecialAttack(SpecialAttackMode mode)
    {
        specialAttackMode = mode;

        base.SetDirtyBit(1);
    }

    void SetBattleMove()
    {
        if (isServer == false)
        {
            return;
        }

        float halfPingpongHeight = 0f;
        float rand = Random.value;

        if (rand < 0.5f)
        {
            halfPingpongHeight = BattleMoveMax * 0.5f * Random.value;
        }
        else
        {
            halfPingpongHeight = -BattleMoveMax * 0.5f * Random.value;
        }


        RpcSetBattleMove(halfPingpongHeight);
    }

    [ClientRpc]
    public void RpcSetBattleMove(float halfPingpongHeight)
    {
        needBattleMove = true;
        TargetPosition = BattleMoveStartPosition;
        TargetPosition.y += halfPingpongHeight;

        CurrentSpeed = 0f;

        base.SetDirtyBit(1);
    }

    void UpdateBattleMove()
    {
        UpdateSpeed();

        float distance = Vector3.Distance(TargetPosition, transform.position);
        if (distance <= Vector3.kEpsilon)
        {
            SetBattleFire();
        }
        else
        {
            transform.position = Vector3.SmoothDamp(transform.position, TargetPosition, ref CurrentVelocity, distance / CurrentSpeed, MaxSpeed * 0.2f);
        }
    }

    void SetBattleFire()
    {
        if (isServer)
        {
            RpcSetBattleFire();
        }
    }

    [ClientRpc]
    public void RpcSetBattleFire()
    {
        needBattleMove = false;
        FireRemainCountPerOneTime = FireRemainCount;

        SpecialAttackCount = MissileFireTransforms.Length;

        base.SetDirtyBit(1);
    }

    [ClientRpc]
    public void RpcSetDirtyBit()
    {
        base.SetDirtyBit(1);
    }

    void FireChase()
    {
        List<Player> alivePlayer = new List<Player>();
        for (int i = 0; i < Players.Length; i++)
        {
            if (Players[i].IsDead == false)
            {
                alivePlayer.Add(Players[i]);
            }
        }

        if (alivePlayer.Count == 0)
        {
            return;
        }

        int index = Random.Range(0, alivePlayer.Count);
        int targetInstanceID = alivePlayer[index].ActorInstanceID;

        Transform missileFireTransform = MissileFireTransforms[MissileFireTransforms.Length - SpecialAttackCount];

        GuidedMissile missile = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().BulletManager.Generate(BulletManager.GuidedMissileIndex, missileFireTransform.position) as GuidedMissile;
        if (missile)
        {
            missile.FireChase(targetInstanceID, actorInstanceID, missileFireTransform.right, MissileSpeed, Damage);
        }
    }

    protected override void OnDead()
    {
        Destroy(beam);

        base.OnDead();

        SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.DieBoss1, soundPrioiry);
        SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.DieBoss2, soundPrioiry);
        SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.DieBoss3, soundPrioiry);

        if (isServer)
        {
            SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().OnGameEnd(true);
        }
    }

    void InitializeHUD()
    {
        InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();

        GameObject go = Resources.Load<GameObject>(BossHUDPath);
        GameObject goInstance = Instantiate<GameObject>(go, inGameSceneMain.DamageManager.CanvasTransform);

        playerHUD = goInstance.GetComponent<PlayerHUD>();
        playerHUD.Initialize(this, false);
    }

    void InitializeBeam()
    {
        GameObject go = Resources.Load<GameObject>(Beam.FilePath);
        GameObject goInstance = Instantiate<GameObject>(go, transform);

        beam = goInstance.GetComponent<Beam>();
        beam.Initialize();
        goInstance.transform.position = FireTransform.position;
    }
}
