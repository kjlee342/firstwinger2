﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Actor : NetworkBehaviour
{
    [SerializeField]
    [SyncVar]
    protected int MaxHP = 100;

    public int HPMax
    {
        get
        {
            return MaxHP;
        }
    }

    [SerializeField]
    [SyncVar]
    protected int CurrentHP;

    public int HPCurrent
    {
        get
        {
            return CurrentHP;
        }
    }

    [SerializeField]
    [SyncVar]
    protected int Damage = 1;

    [SerializeField]
    [SyncVar]
    protected int crashDamage = 100;

    public int CrashDamage
    {
        get
        {
            return crashDamage;
        }
    }

    [SerializeField]
    [SyncVar]
    protected int actorInstanceID = 0;

    public int ActorInstanceID
    {
        get
        {
            return actorInstanceID;
        }
    }

    protected PlayerHUD playerHUD;

    [SerializeField]
    [SyncVar]
    protected bool isDead = false;

    public bool IsDead
    {
        get
        {
            return isDead;
        }
    }

    protected int soundPrioiry = 0;


    ~Actor()
    {
        if (playerHUD != null)
        {
            Destroy(playerHUD.gameObject);
        }
    }

    void Start()
    {
        Initialize();
    }

    void Update()
    {
        UpdateActor();
    }

    protected virtual void UpdateActor()
    {

    }

    protected virtual void Initialize()
    {
        CurrentHP = MaxHP;

        if (isServer)
        {
            actorInstanceID = GetInstanceID();
            RpcSetActorInstanceID(actorInstanceID);
        }
    }


    public virtual void OnBulletHited(int damage, Vector3 position)
    {
        DecreaseHP(damage, position);
    }

    public virtual void OnCrash(int damage, Vector3 position)
    {
        DecreaseHP(damage, position);
    }

    protected virtual void DecreaseHP(int value, Vector3 position)
    {
        if (IsDead)
        {
            return;
        }

        //CmdDecreaseHP(value, position);

        if (isServer)
        {
            RpcDecreaseHP(value, position);
        }
        else
        {
            CmdDecreaseHP(value, position);
            if (isLocalPlayer)
            {
                InternalDecrease(value, position);
            }
        }
    }

    protected virtual void InternalDecrease(int value, Vector3 position)
    {
        if (isDead)
        {
            return;
        }

        CurrentHP -= value;

        if (CurrentHP < 0)
        {
            CurrentHP = 0;
        }

        if (CurrentHP == 0)
        {
            OnDead();
        }
    }

    protected virtual void OnDead()
    {
        isDead = true;

        SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().EffectManager.GenerateEffect(EffectManager.ActorDeadFxIndex, transform.position);
    }

    public void SetPosition(Vector3 position)
    {

        if (isServer)
        {
            RpcSetPosition(position);
        }
        else
        {
            CmdSetPosition(position);
            if (isLocalPlayer)
            {
                transform.position = position;
            }
        }
    }

    [Command]
    public void CmdSetPosition(Vector3 position)
    {
        this.transform.position = position;
        base.SetDirtyBit(1);
    }

    [ClientRpc]
    public void RpcSetPosition(Vector3 position)
    {
        this.transform.position = position;
        base.SetDirtyBit(1);
    }

    [ClientRpc]
    public void RpcSetActive(bool value)
    {
        this.gameObject.SetActive(value);
        base.SetDirtyBit(1);
    }

    [ClientRpc]
    public void RpcSetActorInstanceID(int instanceID)
    {
        this.actorInstanceID = instanceID;

        if (this.actorInstanceID != 0)
        {
            SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().ActorManager.Regist(this.actorInstanceID, this);
        }

        base.SetDirtyBit(1);
    }

    [Command]
    public void CmdDecreaseHP(int value, Vector3 position)
    {
        InternalDecrease(value, position);
        base.SetDirtyBit(1);
    }

    [ClientRpc]
    public void RpcDecreaseHP(int value, Vector3 position)
    {
        InternalDecrease(value, position);
        base.SetDirtyBit(1);
    }
}
