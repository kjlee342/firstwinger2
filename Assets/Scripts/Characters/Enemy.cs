﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

using Random = UnityEngine.Random;

public class Enemy : Actor
{
    public enum State : int
    {
        None = -1,
        Ready = 0,
        Appear,
        Battle,
        Dead,
        Disappear,
    }

    [SerializeField]
    [SyncVar]
    State CurrentState = State.None;

    protected float MaxSpeed = 10.0f;
    const float MaxSpeedTime = 0.5f;

    [SerializeField]
    [SyncVar]
    protected Vector3 TargetPosition;

    [SerializeField]
    [SyncVar]
    protected float CurrentSpeed;

    [SyncVar]
    protected Vector3 CurrentVelocity;

    [SyncVar]
    protected float MoveStartTime = 0f;

    [SerializeField]
    protected Transform FireTransform;

    [SerializeField]
    [SyncVar]
    protected float BulletSpeed = 10f;

    [SyncVar]
    protected float ActionUpdateTime = 0f;

    [SerializeField]
    [SyncVar]
    protected int FireRemainCount = 3;

    [SerializeField]
    [SyncVar]
    int GamePoint = 100;

    [SerializeField]
    [SyncVar]
    string filePath;

    public string FilePath
    {
        get
        {
            return filePath;
        }

        set
        {
            filePath = value;
        }
    }

    [SyncVar]
    Vector3 AppearPoint;
    [SyncVar]
    Vector3 DisappearPoint;

    [SerializeField]
    [SyncVar]
    float ItemDropRate;

    [SerializeField]
    [SyncVar]
    int ItemDropID;

    protected virtual int BulletIndex
    {
        get
        {
            return BulletManager.EnemyBulletIndex;
        }
    }

    protected override void Initialize()
    {
        base.Initialize();

        soundPrioiry = 3;

        InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();

        if (((FWNetworkManager)FWNetworkManager.singleton).isServer == false)
        {
            transform.SetParent(inGameSceneMain.EnemyManager.transform);
            inGameSceneMain.EnemyCacheSystem.Add(filePath, gameObject);
            gameObject.SetActive(false);
        }

        if (actorInstanceID != 0)
        {
            inGameSceneMain.ActorManager.Regist(actorInstanceID, this);
        }
    }

    protected override void UpdateActor()
    {
        switch (CurrentState)
        {
            case State.None:
                break;

            case State.Ready:
                {
                    UpdateReady();
                }
                break;

            case State.Dead:
                break;

            case State.Appear:
            case State.Disappear:
                {
                    UpdateSpeed();
                    UpdateMove();
                }
                break;

            case State.Battle:
                {
                    UpdateBattle();
                }
                break;
        }
    }

    protected void UpdateSpeed()
    {
        CurrentSpeed = Mathf.Lerp(CurrentSpeed, MaxSpeed, (Time.time - MoveStartTime) / MaxSpeedTime);
    }

    void UpdateMove()
    {
        float distance = Vector3.Distance(TargetPosition, transform.position);
        if (distance == 0)
        {
            Arrived();
            return;
        }

        CurrentVelocity = (TargetPosition - transform.position).normalized * CurrentSpeed;

        transform.position = Vector3.SmoothDamp(transform.position, TargetPosition, ref CurrentVelocity, distance / CurrentSpeed, MaxSpeed);
    }

    void Arrived()
    {
        CurrentSpeed = 0f;
        if (CurrentState == State.Appear)
        {
            SetBattleState();
            
        }
        else //if (CurrentState == State.Disappear)
        {
            CurrentState = State.None;
            SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().EnemyManager.RemoveEnemy(this);
        }
    }

    protected virtual void SetBattleState()
    {
        CurrentState = State.Battle;
        ActionUpdateTime = Time.time;
    }

    public void Reset(SquadronMemberStruct data)
    {
        //CmdReset(data);

        if (isServer)
        {
            RpcReset(data);
        }
        else
        {
            CmdReset(data);
            if (isLocalPlayer)
            {
                ResetData(data);
            }
        }
    }

    public void ResetData(SquadronMemberStruct data)
    {
        EnemyStruct enemyStruct = SystemManager.Instance.EnemyTable.GetEnemy(data.EnemyID);
        MaxHP = enemyStruct.MaxHP;
        CurrentHP = enemyStruct.MaxHP;
        crashDamage = enemyStruct.CrashDamage;
        BulletSpeed = enemyStruct.BulletSpeed;
        FireRemainCount = enemyStruct.FireRemainCount;
        GamePoint = enemyStruct.GamePoint;

        AppearPoint = new Vector3(data.AppearPointX, data.AppearPointY, 0);
        DisappearPoint = new Vector3(data.DisappearPointX, data.DisappearPointY, 0);

        ItemDropRate = enemyStruct.ItemDropRate;
        ItemDropID = enemyStruct.ItemDropID;

        CurrentState = State.Ready;
        ActionUpdateTime = Time.time;

        isDead = false;
    }

    public void Appear(Vector3 targetPos)
    {
        TargetPosition = targetPos;
        CurrentSpeed = MaxSpeed;

        CurrentState = State.Appear;
        MoveStartTime = Time.time;
    }

    void Disappear(Vector3 targetPos)
    {
        TargetPosition = targetPos;
        CurrentSpeed = 0;

        CurrentState = State.Disappear;
        MoveStartTime = Time.time;
    }

    void UpdateReady()
    {
        if (1.0f < Time.time - ActionUpdateTime)
        {
            Appear(AppearPoint);
        }
    }

    protected virtual void UpdateBattle()
    {
        float time = Time.time;
        if (1.0f < time - ActionUpdateTime)
        {
            if (0 < FireRemainCount)
            {
                Fire(-FireTransform.right);
                SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.ShotEnemyBullet, soundPrioiry);
                FireRemainCount--;
            }
            else
            {
                Disappear(DisappearPoint);
            }

            ActionUpdateTime = time;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Player player = other.GetComponentInParent<Player>();
        if (player != null)
        {
            Vector3 crashPosition = Vector3.zero;
            if (player.AccurateMode)
            {
                SphereCollider sphere = (SphereCollider)other;
                crashPosition = player.transform.position + sphere.center;
                crashPosition.x += sphere.radius;
                
            }
            else
            {
                BoxCollider box = (BoxCollider)other;
                crashPosition = player.transform.position + box.center;
                crashPosition.x += box.size.x * 0.5f;
            }

            player.OnCrash(CrashDamage, crashPosition);
        }
    }

    public virtual void Fire(Vector3 direction)
    {
        Bullet bullet = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().BulletManager.Generate(BulletIndex, FireTransform.position);
        if (bullet)
        {
            bullet.Fire(actorInstanceID, direction, BulletSpeed, Damage);
        }
    }

    protected override void OnDead()
    {
        base.OnDead();

        InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();
        inGameSceneMain.GamePoint.Add(GamePoint);
        inGameSceneMain.EnemyManager.RemoveEnemy(this);

        SystemManager.Instance.SoundManager.PlayEffect(SoundEffectList.DieActor, soundPrioiry);

        GenerateItem();

        CurrentState = State.Dead;
    }

    protected override void DecreaseHP(int value, Vector3 position)
    {
        base.DecreaseHP(value, position);

        Vector3 damagePoint = position + Random.insideUnitSphere * 0.5f;
        SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().DamageManager.Generate(DamageManager.EnemyDamageIndex, damagePoint, value, Color.magenta);
    }

    [Command]
    public void CmdReset(SquadronMemberStruct data)
    {
        ResetData(data);
        SetDirtyBit(1);
    }

    [ClientRpc]
    public void RpcReset(SquadronMemberStruct data)
    {
        ResetData(data);
        SetDirtyBit(1);
    }

    void GenerateItem()
    {
        if (isServer == false)
        {
            return;
        }

        float ItemGen = Random.Range(0f, 100f);
        if (ItemDropRate < ItemGen)
        {
            return;
        }

        ItemDropTable itemDropTable = SystemManager.Instance.ItemDropTable;
        ItemDropStruct dropStruct = itemDropTable.GetDropData(ItemDropID);

        ItemGen = Random.Range(0, dropStruct.Rate1 + dropStruct.Rate2 + dropStruct.Rate3);

        int itemIndex;

        if (ItemGen <= dropStruct.Rate1)
        {
            itemIndex = dropStruct.ItemID1;
        }
        else if (ItemGen <= dropStruct.Rate1 + dropStruct.Rate2)
        {
            itemIndex = dropStruct.ItemID2;
        }
        else // if (ItemGen <= dropStruct.Rate1 + dropStruct.Rate2 + dropStruct.Rate3)
        {
            itemIndex = dropStruct.ItemID3;
        }

        InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();
        inGameSceneMain.ItemBoxManager.Generate(itemIndex, transform.position);
    }

    public void AddList()
    {
        if (isServer)
        {
            RpcAddList();
        }
    }

    [ClientRpc]
    public void RpcAddList()
    {
        SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().EnemyManager.AddList(this);
        base.SetDirtyBit(1);
    }

    public void RemoveList()
    {
        if (isServer)
        {
            RpcRemoveList();
        }
    }

    [ClientRpc]
    public void RpcRemoveList()
    {
        SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().EnemyManager.RemoveList(this);
        base.SetDirtyBit(1);
    }
}
