﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NetworkConnectionInfo
{
    public bool Host;

    public string IPAddress;

    public int Port;
}
