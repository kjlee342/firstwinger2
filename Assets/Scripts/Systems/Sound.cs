﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public enum SoundState
{
    None = 0,
    Idle,
    Play,
}

public class Sound : MonoBehaviour
{
    public const string FilePath = "Prefabs/Sound";

    [SerializeField]
    AudioSource audioSource = null;

    float playTime = 0f;
    float playedTime = 0f;

    SoundState state = SoundState.None;

    List<(GameObject, Sound)> waitAudioSources;

    private void Update()
    {
        UpdateSound();
    }

    public void Initialize(List<(GameObject, Sound)> wait, AudioMixerGroup mixer)
    {
        waitAudioSources = wait;
        audioSource.outputAudioMixerGroup = mixer;
    }

    public void PlaySound(AudioClip clip, float time = 0f)
    {
        state = SoundState.Play;
        audioSource.clip = clip;
        playTime = time;
        playedTime = Time.time;
        audioSource.Play();
    }

    void UpdateSound()
    {
        if (state == SoundState.Idle || state == SoundState.None)
        {
            return;
        }

        if (audioSource.isPlaying == false)
        {
            state = SoundState.Idle;
            waitAudioSources.Add((gameObject, this));
            enabled = false;
        }
        else if (0 < playTime
            && playedTime + playTime <= Time.time)
        {
            state = SoundState.Idle;
            waitAudioSources.Add((gameObject, this));
            audioSource.Stop();
            enabled = false;
        }
    }
}
