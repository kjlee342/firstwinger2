﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]
public class PrefabCacheData
{
    public string filePath;
    public int cacheCount;
}

public class PrefabCacheSystem
{
    Dictionary<string, Queue<GameObject>> Caches = new Dictionary<string, Queue<GameObject>>();

    public void GenerateCache(string filePath, GameObject gameObejct, int cacheCount, Transform parentTransform = null)
    {
        if (Caches.ContainsKey(filePath))
        {
            return;
        }

        Queue<GameObject> queue = new Queue<GameObject>();
        for (int i = 0; i < cacheCount; i++)
        {
            GameObject go = Object.Instantiate<GameObject>(gameObejct, parentTransform);

            Enemy enemy = go.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.FilePath = filePath;
                NetworkServer.Spawn(go);
                //enemy.RpcSetActiven(false);
            }

            Bullet bullet = go.GetComponent<Bullet>();
            if (bullet != null)
            {
                bullet.FilePath = filePath;
                NetworkServer.Spawn(go);
            }

            ItemBox item = go.GetComponent<ItemBox>();
            if (item != null)
            {
                item.FilePath = filePath;
                NetworkServer.Spawn(go);
            }

            PlayerAddon addon = go.GetComponent<PlayerAddon>();
            if (addon != null)
            {
                addon.FilePath = filePath;
                NetworkServer.Spawn(go);
            }

            go.SetActive(false);
            queue.Enqueue(go);
        }

        Caches.Add(filePath, queue);
    }

    public GameObject Archive(string filePath, Vector3 position)
    {
        if (Caches.ContainsKey(filePath) == false)
        {
            return null;
        }

        if (Caches[filePath].Count == 0)
        {
            Debug.LogError("Cache not enough: " + filePath);
            return null;
        }

        GameObject go = Caches[filePath].Dequeue();
        go.SetActive(true);
        go.transform.position = position;

        if (((FWNetworkManager)FWNetworkManager.singleton).isServer)
        {
            Enemy enemy = go.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.SetPosition(position);
                enemy.RpcSetActive(true);
            }

            Bullet bullet = go.GetComponent<Bullet>();
            if (bullet != null)
            {
                bullet.SetPosition(position);
                bullet.RpcSetActive(true);
            }

            ItemBox item = go.GetComponent<ItemBox>();
            if (item != null)
            {
                item.RpcSetPosition(position);
                item.RpcSetActive(true);
            }

            PlayerAddon addon = go.GetComponent<PlayerAddon>();
            if (addon != null)
            {
                addon.RpcSetPosition(position);
                addon.RpcSetActive(true);
            }
        }

        return go;
    }

    public bool Restore(string filePath, GameObject gameObject)
    {
        if (Caches.ContainsKey(filePath) == false)
        {
            return false;
        }

        gameObject.SetActive(false);

        if (((FWNetworkManager)FWNetworkManager.singleton).isServer)
        {
            Enemy enemy = gameObject.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.RpcSetActive(false);
            }

            Bullet bullet = gameObject.GetComponent<Bullet>();
            if (bullet != null)
            {
                bullet.RpcSetActive(false);
            }

            ItemBox item = gameObject.GetComponent<ItemBox>();
            if (item != null)
            {
                item.RpcSetActive(false);
            }

            PlayerAddon addon = gameObject.GetComponent<PlayerAddon>();
            if (addon != null)
            {
                addon.RpcSetActive(false);
            }
        }

        Caches[filePath].Enqueue(gameObject);

        return true;
    }

    public void Add(string filePath, GameObject gameObject)
    {
        Queue<GameObject> queue;
        if (Caches.ContainsKey(filePath))
        {
            queue = Caches[filePath];
        }
        else
        {
            queue = new Queue<GameObject>();
            Caches.Add(filePath, queue);
        }

        queue.Enqueue(gameObject);
    }
}
