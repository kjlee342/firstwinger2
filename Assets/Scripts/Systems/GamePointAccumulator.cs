﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePointAccumulator
{
    int gamePoint = 0;

    public int GamePoint
    {
        get
        {
            return gamePoint;
        }
    }

    void SetScore(int value)
    {
        PlayerStatePanel playerStatePanel = PanelManager.GetPanel(typeof(PlayerStatePanel)) as PlayerStatePanel;
        playerStatePanel.SetScore(value);
    }

    public void Add(int value)
    {
        gamePoint += value;

        SetScore(gamePoint);
    }

    public void Reset()
    {
        gamePoint = 0;

        SetScore(gamePoint);
    }
}
