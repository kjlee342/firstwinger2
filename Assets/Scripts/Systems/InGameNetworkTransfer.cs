﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum GameState : int
{
    None = 0,
    Ready,
    Running,
    NoInput,
    End,
}

[System.Serializable]
public class InGameNetworkTransfer : NetworkBehaviour
{
    const float GameReadyInterval = 0.5f;

    [SyncVar]
    GameState currentGameState = GameState.None;
    public GameState CurrentGameState
    {
        get
        {
            return currentGameState;
        }
    }

    [SyncVar]
    float CountingStartTime;

    private void Update()
    {
        float currentTime = Time.time;
        if (currentGameState == GameState.Ready)
        {
            if (GameReadyInterval < currentTime - CountingStartTime)
            {
                SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().SquadronManager.StartGame();
                currentGameState = GameState.Running;
            }
        }
    }

    public void RpcGameStart()
    {
        CountingStartTime = Time.time;
        currentGameState = GameState.Ready;

        InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();
        inGameSceneMain.EnemyManager.Prepare();
        inGameSceneMain.BulletManager.Prepare();
        inGameSceneMain.ItemBoxManager.Prepare();
        inGameSceneMain.AddonManager.Prepare();
    }

    [ClientRpc]
    public void RpcShowWarningUI()
    {
        PanelManager.GetPanel(typeof(WarningPanel)).Show();
        currentGameState = GameState.NoInput;
    }

    [ClientRpc]
    public void RpcSetRunningState()
    {
        currentGameState = GameState.Running;
    }

    [ClientRpc]
    public void RpcGameEnd(bool success)
    {
        currentGameState = GameState.End;
        GameEndPanel gameEndPanel = PanelManager.GetPanel(typeof(GameEndPanel)) as GameEndPanel;
        gameEndPanel.ShowGameEnd(success);
    }

    public void SetGameStateEnd()
    {
        currentGameState = GameState.End;
    }
}
