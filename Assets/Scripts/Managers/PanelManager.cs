﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PanelManager : MonoBehaviour
{
    static Dictionary<Type, BasePanel> Panels = new Dictionary<Type, BasePanel>();

    public static bool RegistPanel(Type PanelClassType, BasePanel basePanel)
    {
        if (Panels.ContainsKey(PanelClassType))
        {
            Debug.LogError("already registed panel: " + PanelClassType.ToString());
            return false;
        }

        Panels.Add(PanelClassType, basePanel);

        return true;
    }

    public static bool UnregistPanel(Type PanelClassType)
    {
        if (Panels.ContainsKey(PanelClassType) == false)
        {
            Debug.LogError("not registed panel: " + PanelClassType.ToString());
            return false;
        }

        Panels.Remove(PanelClassType);
        
        return true;
    }

    public static BasePanel GetPanel(Type PanelClassType)
    {
        if (Panels.ContainsKey(PanelClassType) == false)
        {
            Debug.LogError("not found panel: " + PanelClassType.ToString());
            return null;
        }

        return Panels[PanelClassType];
    }
}
