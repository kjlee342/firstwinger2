﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddonManager : MonoBehaviour
{
    [SerializeField]
    PrefabCacheData[] Files;

    Dictionary<string, GameObject> FileCache = new Dictionary<string, GameObject>();

    public GameObject Load(string resourcePath)
    {
        GameObject go = null;

        if (FileCache.ContainsKey(resourcePath))
        {
            go = FileCache[resourcePath];
        }
        else
        {
            go = Resources.Load<GameObject>(resourcePath);
            if (go == null)
            {
                Debug.LogError("addon load error path = " + resourcePath);
                return null;
            }

            FileCache.Add(resourcePath, go);
        }

        return go;
    }

    public void Prepare()
    {
        if (((FWNetworkManager)FWNetworkManager.singleton).isServer == false)
        {
            return;
        }

        for (int i = 0; i < Files.Length; i++)
        {
            GameObject go = Load(Files[i].filePath);
            SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().AddonCacheSystem.GenerateCache(Files[i].filePath, go, Files[i].cacheCount, transform);
        }
    }

    public GameObject Generate(int index, Vector3 position)
    {
        if (index < 0 || Files.Length <= index)
        {
            Debug.LogError("addon generate error index = " + index);
            return null;
        }

        string filePath = Files[index].filePath;
        GameObject go = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().AddonCacheSystem.Archive(filePath, position);

        PlayerAddon addon = go.GetComponent<PlayerAddon>();
        addon.FilePath = filePath;

        return go;
    }

    public bool Remove(PlayerAddon addon)
    {
        SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().AddonCacheSystem.Restore(addon.FilePath, addon.gameObject);

        return true;
    }
}
