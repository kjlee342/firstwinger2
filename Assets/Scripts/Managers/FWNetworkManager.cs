﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FWNetworkManager : NetworkManager
{
    public const int WaitingPlayerCount = 2;

    int PlayerCount = 0;

    public bool isServer
    {
        get;
        private set;
    }

    #region SERVER SIDE EVENET

    public override void OnServerConnect(NetworkConnection conn)
    {
        //Debug.Log("OnServerConnect + " + conn.address + ", " + conn.connectionId);
        base.OnServerConnect(conn);
    }

    public override void OnServerSceneChanged(string sceneName)
    {
        //Debug.Log("OnServerConnect + " + sceneName);
        base.OnServerSceneChanged(sceneName);
    }

    public override void OnServerReady(NetworkConnection conn)
    {
        //Debug.Log("OnServerReady + " + conn.address + ", " + conn.connectionId);
        base.OnServerReady(conn);

        PlayerCount++;

        if (WaitingPlayerCount <= PlayerCount)
        {
            InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();
            if (inGameSceneMain.CurrentGameState == GameState.None)
            {
                inGameSceneMain.GameStart();
            }

            PanelManager.GetPanel(typeof(NetworkWaitPanel)).Close();
        }
    }

    public override void OnServerError(NetworkConnection conn, int errorCode)
    {
        //Debug.Log("OnServerError + " + errorCode);
        base.OnServerError(conn, errorCode);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        //Debug.Log("OnServerDisconnect + " + conn.address);
        base.OnServerDisconnect(conn);
    }

    public override void OnStartServer()
    {
        //Debug.Log("OnStartServer");
        base.OnStartServer();
        isServer = true;
    }

    #endregion

    #region CLIENT SIDE EVENT

    public override void OnStartClient(NetworkClient client)
    {
        //Debug.Log("OnStartClient + " + client.serverIp);
        base.OnStartClient(client);
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        //Debug.Log("OnClientConnect + " + conn.address);
        base.OnClientConnect(conn);
    }

    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        //Debug.Log("OnClientSceneChanged + " + conn.address);
        base.OnClientSceneChanged(conn);
    }

    public override void OnClientError(NetworkConnection conn, int errorCode)
    {
        //Debug.Log("OnClientError + " + errorCode);
        base.OnClientError(conn, errorCode);
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        //Debug.Log("OnClientDisconnect + " + conn.address);

        if (isServer == false)
        {
            InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();
            if (inGameSceneMain.CurrentGameState == GameState.End)
            {
                base.OnClientDisconnect(conn);

                inGameSceneMain.GotoTitleScene();
                return;
            }
        }

        base.OnClientDisconnect(conn);
    }

    public override void OnClientNotReady(NetworkConnection conn)
    {

        //Debug.Log("OnClientNotReady + " + conn.address);
        base.OnClientNotReady(conn);
    }

    public override void OnDropConnection(bool success, string extendedInfo)
    {

        //Debug.Log("OnDropConnection + " + extendedInfo);
        base.OnDropConnection(success, extendedInfo);
    }

    #endregion
}
