﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorManager
{
    Dictionary<int, Actor> Actors = new Dictionary<int, Actor>();

    public bool Regist(int ActorInstanceID, Actor actor)
    {
        if (ActorInstanceID == 0)
        {
            Debug.LogError("Regist error, ActorInstanceID = 0");
            return false;
        }

        if (Actors.ContainsKey(ActorInstanceID))
        {
            if (actor.GetInstanceID() != Actors[ActorInstanceID].GetInstanceID())
            {
                Debug.LogError("Regist error, already exist, ActorInstanceID = " + ActorInstanceID);
                return false;
            }

            return true;
        }

        Actors.Add(ActorInstanceID, actor);

        return true;
    }

    public Actor GetActor(int ActorInstanceID)
    {
        if (Actors.ContainsKey(ActorInstanceID) == false)
        {
            Debug.LogError("GetActor error, no exist, ActorInstanceID = " + ActorInstanceID);
            return null;
        }

        return Actors[ActorInstanceID];
    }
}
