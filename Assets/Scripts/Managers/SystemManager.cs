﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemManager : MonoBehaviour
{
    static SystemManager instace = null;

    public static SystemManager Instance
    {
        get
        {
            return instace;
        }
    }

    [SerializeField]
    SoundManager soundManager;

    public SoundManager SoundManager
    {
        get
        {
            return soundManager;
        }
    }

    [SerializeField]
    EnemyTable enemyTable;

    public EnemyTable EnemyTable
    {
        get
        {
            return enemyTable;
        }
    }

    [SerializeField]
    ItemTable itemTable;

    public ItemTable ItemTable
    {
        get
        {
            return itemTable;
        }
    }

    [SerializeField]
    ItemDropTable itemDropTable;

    public ItemDropTable ItemDropTable
    { 
        get
        {
            return itemDropTable;
        }
    }

    BaseSceneMain currentSceneMain;

    public BaseSceneMain CurrentSceneMain
    {
        get
        {
            return currentSceneMain;
        }
        set
        {
            currentSceneMain = value;
        }
    }

    [SerializeField]
    NetworkConnectionInfo connectionInfo = new NetworkConnectionInfo();

    public NetworkConnectionInfo ConnectionInfo
    {
        get
        {
            return connectionInfo;
        }
    }

    private void Awake()
    {
        if (instace != null)
        {
            Debug.LogError("SystemManager singletone error");
            Destroy(gameObject);
            return;
        }

        instace = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        BaseSceneMain baseSceneMain = GameObject.FindObjectOfType<BaseSceneMain>();
        SystemManager.Instance.CurrentSceneMain = baseSceneMain;
    }

    public T GetCurrentSceneMain<T>() where T : BaseSceneMain
    {
        return currentSceneMain as T;
    }
}
