﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System.Linq;

public enum SoundEffectList : int
{
    BGM = 0,

    ShotPlayerBullet,
    ShotPlayerBomb,

    ShotEnemyBullet,
    ShotBossBullet,
    ShotBossMissile,

    ImpactPlayerBullet,
    ExplodePlayerBomb,

    ImpactEnemyBullet,
    ImpactBossBullet,
    ExplodeBossMissile,

    DieActor,
    DieBoss1,
    DieBoss2,
    DieBoss3,

    ShotBossBeam,
    AlertBoss,
}

[System.Serializable]
public class SerializePlayTime : SerializableDictionary<int, float> { }

class Comparer : IComparer<(int, SoundEffectList)>
{
    public int Compare((int, SoundEffectList) x, (int, SoundEffectList) y)
    {
        if (x.Item1 < y.Item1)
        {
            return 1;
        }
        else if (x.Item1 == y.Item1)
        {
            return 0;
        }

        return -1;
    }
}

public class SoundManager : MonoBehaviour
{
    const int NumberOfSources = 5;

    [SerializeField]
    AudioMixer audioMixer;

    AudioSource BGMPlayer = null;

    [SerializeField]
    AudioClip[] AudioClips;

    public SerializePlayTime playTime = new SerializePlayTime();

    List<(GameObject, Sound)> waitAudioSources = null;

    SortedSet<(int, SoundEffectList)> queue = new SortedSet<(int, SoundEffectList)>(new Comparer());

    private void Awake()
    {
        if (BGMPlayer == null)
        {
            BGMPlayer = gameObject.AddComponent<AudioSource>();

            AudioMixerGroup[] BGMMixer = audioMixer.FindMatchingGroups("BGM");
           
            BGMPlayer.outputAudioMixerGroup = BGMMixer[0];
            //audioMixer.SetFloat("BGMVolume", -20f); // unity 2019 bug (https://forum.unity.com/threads/audiomixer-setfloat-doesnt-work-on-awake.323880/)

            BGMPlayer.clip = AudioClips[(int)SoundEffectList.BGM];
            BGMPlayer.loop = true;
            BGMPlayer.Play();
        }
        
        if (waitAudioSources == null)
        {
            AudioMixerGroup[] effectMixer = audioMixer.FindMatchingGroups("Effect");
            
            waitAudioSources = new List<(GameObject, Sound)>();
            for (int i = 0; i < NumberOfSources; i++)
            {
                GameObject go = Resources.Load<GameObject>(Sound.FilePath);
                GameObject goInstance = Instantiate<GameObject>(go, transform);

                Sound sound = goInstance.GetComponent<Sound>();

                sound.Initialize(waitAudioSources, effectMixer[0]);

                sound.enabled = false;
                waitAudioSources.Add((go, sound));
            }
        }
    }

    private void Update()
    {
        UpdateSound();
    }

    void UpdateSound()
    {
        if (queue.Count == 0 || waitAudioSources.Count == 0)
        {
            return;
        }

        var key = queue.First();
        int Index = (int)key.Item2;

        queue.Remove(key);

        var source = waitAudioSources.Last();
        waitAudioSources.Remove(source);

        float indexPlayTime = playTime.ContainsKey(Index) ? playTime[Index] : 0;

        source.Item2.enabled = true;
        source.Item2.PlaySound(AudioClips[Index], indexPlayTime);
    }


    public void PlayEffect(SoundEffectList value, int priority = 0)
    {
        if (queue.Contains((priority, value)) == false)
        {
            queue.Add((priority, value));
        }
    }
}
