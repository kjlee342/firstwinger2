﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquadronManager : MonoBehaviour
{
    float GameStatedTime;

    int ScheduleIndex;

    [SerializeField]
    SquadronTable[] squadronDatas;

    [SerializeField]
    SquadronScheduleTable squadronScheduleTable;

    bool running = false;

    bool AllSquadronGenerated = false;
    bool ShowWarningUICalled = false;

    private void Start()
    {
        squadronDatas = GetComponentsInChildren<SquadronTable>();
        for (int i = 0; i < squadronDatas.Length; i++)
        {
            squadronDatas[i].Load();
        }

        squadronScheduleTable.Load();
    }

    void Update()
    {
        if (AllSquadronGenerated == false)
        {
            CheckSquardronGeneratings();
        }
        else if (ShowWarningUICalled == false)
        {
            InGameSceneMain inGameSceneMain = SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>();
            if (inGameSceneMain.EnemyManager.GetEnemyListCount() == 0)
            {
                inGameSceneMain.ShowWarningUI();
                ShowWarningUICalled = true;
            }
        }
    }

    public void StartGame()
    {
        GameStatedTime = Time.time;
        ScheduleIndex = 0;
        running = true;
    }

    void CheckSquardronGeneratings()
    {
        if (running == false)
        {
            return;
        }

        SquadronScheduleDataStruct data = squadronScheduleTable.GetScheduleData(ScheduleIndex);

        if (data.GenerateTime <= Time.time - GameStatedTime)
        {
            GenerateSquardron(squadronDatas[data.SquadronID]);
            ScheduleIndex++;

            if (squadronScheduleTable.GetDataCount() <= ScheduleIndex)
            {
                OnAllSquadronGenerated();
                return;
            }
        }
    }

    void GenerateSquardron(SquadronTable table)
    {
        for (int i = 0; i < table.GetCount(); i++)
        {
            SquadronMemberStruct squadronMember = table.GetSquadronMember(i);
            SystemManager.Instance.GetCurrentSceneMain<InGameSceneMain>().EnemyManager.GenerateEnemy(squadronMember);
        }
    }

    void OnAllSquadronGenerated()
    {
        running = false;
        AllSquadronGenerated = true;
    }
}
